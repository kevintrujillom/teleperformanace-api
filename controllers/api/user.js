const userService = require('../../services/user');

const { hashPassword } = require('../../utils/mongoUtils');

const controller = {}

controller.getAll = async (req, res) => {
    
    try {
        const users = await userService.getAll();

        return res.status(200).json(users);
    
    } catch (error) {

        return res.status(500).json({ error: "Internal server error" });
    }
}

controller.getByID = async (req, res) => {

    try {
        const id = req.params.id;
        const user = await userService.getByID(id);

        if (!user.success) {
            return res.status(409).json(user.content);
        }

        return res.status(200).json(user);
    
    } catch (error) {

        return res.status(500).json({ error: "Internal server error" });
    }   
}

controller.create =  async (req, res) => {
    
    const { name, email, password, rol } = req.body;
    const passCrypt = hashPassword(password);
    const data = { name, email, password:passCrypt, rol };

    try {
        const emailValidation = await userService.verifyUserExistByEmail(email);

        if (emailValidation.exist) {
            return res.status(200).json(emailValidation);
        }

        const user = await userService.create(data);

        if (!user.success) {
            return res.status(200).json(user);
        }

        return res.status(200).json(user);
    
    } catch (error) {
    
        return res.status(500).json({ error: "Internal server error" });
    }       

}

controller.update = async (req, res) => {

    const id = req.params.id;
    const { name, email, password, rol } = req.body;
    const data = { name, email, password, rol };

    try {
        const userFound = await userService.getByID(id);

        if (!userFound.success) {
            return res.status(409).json(userFound.content);
        }

        if (password == null) {

            data.password = userFound.content.user.password;

        } else if (password != userFound.content.user.password) {

            const passCrypt = hashPassword(password);
            data.password = passCrypt;
        }

        if (email !== userFound.content.user.email) {
            const emailValidation = await userService.verifyUserExistByEmail(email);

            if (emailValidation.exist) {
                return res.status(200).json(emailValidation);
            }
        }

        const userUpdate = await userService.update(userFound.content.user, data);

        return res.status(200).json(userUpdate);
    
    } catch (error) {
    
        return res.status(500).json({ error: "Internal server error" });
    }
}

controller.delete = async (req, res) => {

    const id = req.params.id;
    
    try {
        const userFound = await userService.getByID(id);

        if (!userFound.success) {
            return res.status(409).json(userFound.content);
        }

        const userDeleted = await userService.delete(userFound.content.user);

        return res.status(200).json(userDeleted);

    } catch (error) {

        return res.status(500).json({ error: "Internal server error" });
    }
}

module.exports = controller;