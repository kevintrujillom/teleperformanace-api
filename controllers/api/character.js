const characterService = require('../../services/character');

const controller = {}

controller.getAll = async (req, res) => {
    
    try {
        const characters = await characterService.getAll();

        return res.status(200).json(characters);
    
    } catch (error) {

        return res.status(500).json({ error: "Internal server error" });
    }
}

module.exports = controller;