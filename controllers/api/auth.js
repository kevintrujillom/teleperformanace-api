const authService = require('../../services/auth');

const { checkPassword } = require('../../utils/mongoUtils');

const controller = {}

controller.login = async (req, res) => {

    let controllerResponse = {
        success: true,
        content: {}
    };

    try {
        const userExist = await authService.login(req.body);

        if (!userExist.success) {
            return res.status(404).json(userExist);
        }

        const userLogged = userExist.content.user;
        const passToValidate = checkPassword(req.body.password, userLogged.password);
        
        if (passToValidate) {

            controllerResponse = {
                success: true,
                content: {
                    user: userLogged
                }
            };

            return res.status(202).json(controllerResponse);

        } else {

            controllerResponse = {
                success: false,
                content: {
                    message: "Email or password incorrect"
                }
            };                    
            
            return res.status(404).json(controllerResponse);
        }

    } catch (error) {

        return res.status(500).json({ error: "Internal server error" });
    }  
} 

module.exports = controller;