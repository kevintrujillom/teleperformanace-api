# Teleperformance

## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado teleperformance-api/ donde se encuentra el API desarrollado con el framework Express js. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
npm install
```

## Iniciar servidor
Para iniciar el servidor, ejecutar el siguiente comando:
```
npm start
```
El API debe ser consumida en el siguiente enlace:
```
http://localhost:7001/
```
Para verificar el correcto funcionamiento del API, abrir el navegador de preferencia e ingresar su enlace. Se debe ver el mensaje:
```
Express
Welcome to Express
```
Otra alternativa para verificar el funcionamiento, es ingresar al siguiente enlace donde se debe ver el listado de usuarios (en caso de que existan registros en la base de datos) de la aplicación:
```
http://localhost:7001/api/users/
```
# Tecnologías:
## Implementadas en el API (Backend):
```
1. Express js (Nodejs)
2. MongoDB (Base de datos NoSQL)
```
## Implementadas en el control de versiones:
```
1. Git
2. Bitbucket (repositorio)
```