const express = require('express');
const router = express.Router();

const UserController = require('../../controllers/api/user');

router.get("/", UserController.getAll);
router.get("/:id", UserController.getByID);
router.post("/", UserController.create);
router.put("/:id", UserController.update);
router.delete("/:id", UserController.delete);

module.exports = router;