const express = require('express');
const router = express.Router();

const CharacterController = require('../../controllers/api/character');

router.get("/", CharacterController.getAll);

module.exports = router;