const express = require('express');
const router  = express.Router();

const AuthRouter = require('./api/auth');
const UserRouter = require('./api/user');
const CharacterRouter = require('./api/character');

router.use("/auth", AuthRouter);
router.use("/users", UserRouter);
router.use("/characters", CharacterRouter);

module.exports = router;