const mongoose = require("mongoose");

const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');
const jwtsecret = process.env.JWTSECRET || "secret";
const jwttimeexpire = process.env.JWTTIMEEXPIRE || "1D";

const tools = {};

/*
* Mongoose "setea" automaticamente el id de las entidades
* isValid verifica que el _id exista y sea generado por mongoose
*/
tools.verifyID = (_id) => {

    if ( !_id ) {
        return false;
    }

    if ( !mongoose.Types.ObjectId.isValid(_id) ) {
        return false;
    }

    return true;
}

tools.verifyNumber = (...nums) => {

    const auxArray = nums.map(num => isNaN(parseInt(num)));
    return !auxArray.some(element => element === true);
}


/*
* En este momento crea el token con el id del usuario
* generado por mongoose
*/

/*
* createToken para trabajar con mongoose
*
tools.createToken = (_id) => {
    
    const payload = {
        _id
    }

    return jwt.sign(payload, jwtsecret, {
        expiresIn: jwttimeexpire,
    });
}
*/


tools.generateToken = (username) => {
    
    const payload = {
        username
    }

    let token = jwt.sign(
        payload, 
        jwtsecret, 
        { expiresIn: jwttimeexpire }
    )

    return token
}

tools.verifyToken = (token) => {
    
    try {
        
        return jwt.verify(token, jwtsecret);

    } catch (error) {
        
        return false;
    }
}

tools.hashPassword = (password) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
}

tools.checkPassword = (passwordToValidate, password) => {
    return bcrypt.compareSync(passwordToValidate, password);
}


module.exports = tools;