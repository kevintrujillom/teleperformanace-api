const mongoose = require('mongoose')

// Conexiòn
const options = {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false
}

const connection = mongoose.connect(process.env.MONGO_URL, options).then(() => {
    console.log('Conexión exitosa!');
})

module.exports = {
    connection
}