const { verifyToken } = require('../utils/mongoUtils');

const middleware = {}

middleware.verifyAuth = async (req, res, next) => {

    const { authorization } = req.headers;

    if (!authorization) {
        return res.status(403).json({
            error: "Authorization is required"
        });
    }

    const [prefix, token] = authorization.split(" ");

    if (prefix !== "Bearer") {
        return res.status(400).json({
            error: "Incorrect prefix"
        });
    }

    const tokenObject = verifyToken(token);

    if ( !tokenObject) {
        return res.status(401).json({
            error: "Incorrect token"
        });
    }

    /*
    * Validacion de usuario
    * Se devuelve al usuario autenticado para poder "usarlo"
    * en el userController
    */
    /*
    const userExist = await userService.verifyUserExistByID(userID);
    
    if (!userExist.success) {
        return res.status(409).json(userExist.content);
    }

    req.user = userExist.content;
    */
    next();
}

module.exports = middleware;