const axios = require('axios');

const service = {}

service.getAll = async () => {
    
    let serviceResponse = {
        success: true,
        content: {}
    }
    
    try {

        const characters = await axios.get(process.env.API_RICK_AND_MORTY);

        if (!characters.data) {

            serviceResponse = {
                success: false,
                content: {
                    message: "Characters not found"
                }
            }

        } else {

            serviceResponse.content = {
                characters: characters.data
            }
        }

        return serviceResponse;

    } catch(error){

        throw error;       
    }    

}

module.exports = service;