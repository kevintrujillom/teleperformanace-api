const UserModel = require('../models/user')

const passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[4,32])");
const emailRegex = new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");

const service = {}

service.verifyUserExistByEmail = async (email) => {

    let serviceResponse = {
        exist: true,
        content: {
            message: "Email already exist"
        }
    }

    try {
        const user = await UserModel.findOne({ email: email }).exec();

        if (!user) {

            serviceResponse = {
                exist: false,
                content: {}
            }  

        }       

        return serviceResponse;

    } catch (error) {

        throw error;
    }
}

/*
* CRUD del modelo
*
*/
service.getAll = async () => {
    
    let serviceResponse = {
        success: true,
        content: {}
    }
    
    try {

        const users = await UserModel.find({}).sort('-_id').exec();

        if (!users) {

            serviceResponse = {
                success: false,
                content: {
                    message: "Users not found"
                }
            }

        } else {

            serviceResponse.content = {
                users
            }
        }

        return serviceResponse;

    } catch(error){

        throw error;       
    }    

}

service.getByID = async (id) => {
    
    let serviceResponse = {
        success: true,
        content: {}
    }

    try {

        const user = await UserModel.findById(id).exec();

        if (!user) {

            serviceResponse = {
                success: false,
                content: {
                    message: "User not found"
                }
            }            
        } else {

            serviceResponse.content = {
                user
            };
        }

        return serviceResponse;

    } catch(error){

        throw error;       
    }
}

service.create = async ({ name, email, password, rol }) => {

    let serviceResponse = {
        success: true,
        content: {
            message: "Users created"
        }
    }
    
    try {

        const user = new UserModel({
            name,
            email,
            password,
            rol
        });

        const userSaved = await user.save();

        if (!userSaved) {

            serviceResponse = {
                success: false,
                content: {
                    message: "Users not created"
                }
            }

        }

        serviceResponse.content.user = user

        return serviceResponse; 

    } catch(error){

        return error;       
    }      
}

service.update = async (user, contentToUpdate) => {

    let serviceResponse = {
        success: true,
        content: {
            message: "User updated"
        }
    }

    try {
        // Recorro los campos que venga del form y sobre escribo solo los campos a actulizar
        Object.keys(contentToUpdate).forEach(key => {
            user[key] = contentToUpdate[key];
        });

        const userUpdated = await user.save();

        // Busca un post por ID y actualiza el documento
        // const userUpdated = await UserModel.findByIdAndUpdate(user.id, {
        //     ...contentToUpdate
        // });

        if (!userUpdated) {

            serviceResponse = {
                success: false,
                content: {
                    error: "User not updated"
                }
            }             

        }

        serviceResponse.content.user = userUpdated;

        return serviceResponse;

    } catch(error){

        throw error;       
    }    
}

service.delete = async (user) => {

    let serviceResponse = {
        success: true,
        content: {
            message: "User deleted"
        }
    }

    try {
        const userDeleted = await UserModel.findByIdAndDelete(user.id).exec();

        if (!userDeleted) {

            serviceResponse = {
                success: false,
                content: {
                    error: "User not deleted"
                }
            }             

        }

        return serviceResponse;

    } catch(error){

        throw error;       
    }    
}

module.exports = service;