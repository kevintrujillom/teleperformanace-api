const UserModel = require('../models/user')

const service = {}

service.login = async ({ email }) => {
    
    let serviceResponse = {
        success: true,
        content: {}
    }
    
    try {

        const user = await UserModel.findOne({email: email}).exec();

        if (!user) {

            serviceResponse = {
                success: false,
                content: {
                    message: "Email or password incorrect"
                }
            }

        } else {

            serviceResponse.content = {
                user
            }
        }

        return serviceResponse; 

    } catch(error){

        return error;       
    }    

}

module.exports = service;